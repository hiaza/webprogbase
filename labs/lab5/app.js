const express = require('express');
const path = require('path');
// const favicon = require('static-favicon');
// const logger = require('morgan');
// const cookieParser = require('cookie-parser');
// const bodyParser = require('body-parser');
// const consolidate = require('consolidate');
// const routes = require('./routes/index');
// const users = require('./routes/users');
// const user = require('./routes/user');
// const about = require('./routes/about');
// const tests = require('./routes/tests');
// const test = require('./routes/test');
const user_c = require('./models/user');
const test_c = require('./models/test');
const question_c = require('./models/question');
const fs = require('fs-promise');


const bodyParser = require('body-parser');
const mustache = require('mustache-express');
const busboyBodyParser = require('busboy-body-parser');

const viewsDir = path.join(__dirname, 'views');

const app = express();
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.use(busboyBodyParser({ limit: '5mb' }));

const usersRouter = require('./routes/users');
app.use('/users',usersRouter);
const apisRouter = require('./routes/apis');
app.use('/api',apisRouter);
const testRouter = require('./routes/tests');
app.use('/tests',testRouter);
const questRouter = require('./routes/questions');
app.use('/questions',questRouter);


app.get('/', function(req, res) {
    res.render('index', {  });
});
app.get('/about', function(req, res) {
    res.render('about', {  });
});


app.post('/delete_Question/:id',function(req, res) {
    
    const id = req.params.id;
    
    question_c.delete(id)
              .then(()=>{
                res.redirect("../questions");
              })
              .catch((err)=>res.status(404).send(err.message)) 

});

app.post('/update_Question/:id',function(req, res) {
    
    const id = req.params.id;
    
    question_c.getById(id)
              .then((choosed_question)=>{
                const questionData = {
                    
                        "id":           choosed_question.id,
                        "question":     choosed_question.question,
                        "points":       choosed_question.points,
                        "trueAnswer":   choosed_question.trueAnswer,
                        "fakeAnswer1":  choosed_question.fakeAnswer1,
                        "fakeAnswer2":  choosed_question.fakeAnswer2,
                        "fakeAnswer3":  choosed_question.fakeAnswer3,
                        "fakeAnswer4":  choosed_question.fakeAnswer4,
                    };    
    
                    res.setHeader('Content-Type', 'text/html');
                    res.render('question_editing', questionData);
              })
              .catch((err)=>res.status(404).send(err.message)) 

});

app.get('/files/:name',function(req, res) {
    let Path = path.join(__dirname, '/data/fs/'+ req.params.name);
    fs.readFile(Path)
              .then((pic)=>{res.send(pic);})
              .catch(err=>{res.status(404).send(err.message);})
});


app.post('/files',function(req, res) {
    
        let test_pic = req.files.test_pic;        
        let newPath = path.join(__dirname, 'data/fs/'+test_pic.name);
        
        fs.writeFile(newPath,test_pic.data)
        .then(()=>{
                console.log(req.body.id);
                return test_c.getById(req.body.id)})
        .then((test)=>{
                        test.picUrl = "../files/" + test_pic.name;
                        return Promise.all([test,test.save()]); 
                  })
        .then(([test,])=>{res.redirect("../tests/"+test.id);})

        .catch(err => res.status(404).send(err.message));            
});


app.post('/delete_Test/:id',function(req, res) {
     
    const id = req.params.id;
    
    test_c.delete(id)
              .then(()=>{
                res.redirect("../tests");
              })
              .catch((err)=>res.status(404).send(err.message))  
 });





module.exports = app;
