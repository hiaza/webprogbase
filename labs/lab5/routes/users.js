const express = require('express');
const user_c = require('../models/user');

const router = express.Router();

router.get('/',function(req, res,next) {
    console.log(`got some request`);
    user_c.getAll()
          .then((users_all)=>{
            const usersData = {users_all};
            res.setHeader('Content-Type', 'text/html');    
            res.render('users', usersData);
          })
          .catch((err)=>res.status(404).send(err.message))
});

router.get('/:id',function(req, res,next) {
    console.log(`got some request: ${req.params.id}`);
    user_c.getById(req.params.id)
        .then((choosed_user)=>{ 
                const usersData = {
                    "bio":choosed_user.bio,
                    "fullname":choosed_user.fullname,
                    "login":choosed_user.login,
                    "avaUrl":choosed_user.avaUrl,
                    "dateOfBirth":choosed_user.dateOfBirth
                };    

                res.setHeader('Content-Type', 'text/html');
                res.render('user', usersData);
        })
       
        .catch((err)=>res.status(404).send("Error 404"))  
    
});

module.exports = router;