const express = require('express');
const user_c = require('../models/user');
const test_c = require('../models/test');
const question_c = require('../models/question');
const router = express.Router();

router.get('/users',function(req, res,next) {
    user_c.getAll()
    .then((data) =>{
        const resultString = JSON.stringify(data,null,4);
        res.setHeader('Content-Type', 'application/json');
        res.end(resultString);    
    })
    .catch((err)=> res.status(404).send(err.message))

});

router.get('/users/:id',function(req, res,next) {
    question_c.getById(req.params.id)
    .then((choosed_user)=>{
           res.setHeader('Content-Type', 'application/json');
           res.end(JSON.stringify(choosed_user,null,4));
       })
    .catch ((err)=> res.status(404).send("Error 404"))

});

router.get('/questions',function(req, res,next) {
    question_c.getAll()
    .then((data) =>{
        const resultString = JSON.stringify(data,null,4);
        res.setHeader('Content-Type', 'application/json');
        res.end(resultString);    
    })
    .catch((err)=> res.status(404).send(err.message))
});

router.get('/questions/:id',function(req, res,next) {
    question_c.getById(req.params.id)
    .then((choosed_question)=>{
           res.setHeader('Content-Type', 'application/json');
           res.end(JSON.stringify(choosed_question,null,4));
       })
    .catch ((err)=> res.status(404).send("Error 404"))
});

    
router.get('/tests',function(req, res,next) {
    test_c.getAll()
        .then((data) =>{
            const resultString = JSON.stringify(data,null,4);
            res.setHeader('Content-Type', 'application/json');
            res.end(resultString);    
        })
        .catch((err)=> res.status(404).send(err.message))
});

router.get('/tests/:id(\\d+)',function(req, res,next) {
         test_c.getById(req.params.id)
         .then((choosed_test)=>{
                res.setHeader('Content-Type', 'application/json');
                res.end(JSON.stringify(choosed_test,null,4));
            })
         .catch ((err)=> res.status(404).send("Error 404"))
    })

module.exports = router;
