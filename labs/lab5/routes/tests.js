const express = require('express');

const user_c = require('../models/user');
const test_c = require('../models/test');
const question_c = require('../models/question');
const path = require('path');

const router = express.Router();

const extra = require('../models/extra');

router.get('/new', function(req, res) {
    question_c.getAll()
    .then((data)=>{
        const QData = {"all_items":data};
        res.render('test_creation',QData);
    })
    .catch((err)=> res.status(404).send(err.mes))
});

router.post('/new',function(req, res) {
            
        let authorID = req.body.test_author_id; 
            let questions = [];
            let numOfQuestions = 0;

            for(let question of req.body.test_questions){
                    questions[numOfQuestions] = question;
                    numOfQuestions++;
            }

            let temp = new test_c(      req.body.test_name,
                questions,              '../',
                authorID,               req.body.test_subject,  
                req.body.test_rating,   0,                   
                numOfQuestions
            );
    
            test_c.insert(temp)
                .then((id)=>{res.render("picture_adding",{"tempId": id})})
                .catch((err)=>res.status(404).send(err.message))     
        
});


router.get('/',function(req, res,next) {
    
        test_c.getAll()
            .then((data)=>extra.pagination(res,req,'tests',data,5))
            .catch((err)=> res.status(404).send(err.mes))

});

router.get('/:id',function(req, res) {
    test_c.getById(req.params.id)
        .then((choosed_test)=>{
            return Promise.all([choosed_test,user_c.getById(choosed_test.authorID)])})
        .then(([choosed_test,author])=>{
            return Promise.all([choosed_test,author,question_c.getAll()])})
        .then(([choosed_test,author,pack])=>{
                let ret = [];
                for(let question of pack){
                    let i = 0;
                    while(i<question.tests.length){
                        if(question.tests[i].toString()==choosed_test.id.toString()) {
                            ret.push(question);
                            break;
                        }
                        i++;
                    }
                }
                const testData = {
        
                    "id":                   choosed_test.id,
                    "name":                 choosed_test.name,
                    "picUrl":               choosed_test.picUrl,
                    "author":               author.login,
                    "authorUrl":            "../users/"+author.id,
                    "created":              choosed_test.created,
                    "rating":               choosed_test.rating,
                    "subject":              choosed_test.subject,
                    "numOfpeopleWhoPassed": choosed_test.numOfpeopleWhoPassed,
                    "numOfQuestions":       choosed_test.numOfQuestions,
                    "all_items":            ret             
                };    
                res.setHeader('Content-Type', 'text/html');
                res.render('test', testData);
            }) 
    .catch((err)=>res.status(404).send(err))
});
  
module.exports = router;