'use strict';
const fs = require('fs');
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const User_S = new Schema({
    login: {type:String,required:true},
    role: {type:Number,required:true},
    fullname: {type:String,required:true},
    registeredAt: {type:Date, default:Date.now },
    avaUrl: String,
    isDisabled: Boolean,
    bio: String,
    dateOfBirth: String,
    tests: [{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Test'}]
  });
  
  const UserModel = mongoose.model('User',User_S);

/*{
    author.save(function (err) {
      if (err) return handleError(err);
    
      var story1 = new Story({
        title: 'Casino Royale',
        author: author._id    // assign the _id from the person
      });
    
      story1.save(function (err) {
        if (err) return handleError(err);
        // thats it!
      });
    
}*/

/*
Story.
findOne({ title: 'Casino Royale' }).
populate('author').
exec(function (err, story) {
  if (err) return handleError(err);
  console.log('The author is %s', story.author.name);
  // prints "The author is Ian Fleming"
});
*/
class User{
    constructor (id, login, role, fullname, avaUrl, isDisabled,bio,dateOfBirth) {
        this.id = id;
        this.login = login;
        this.role = role;
        this.fullname = fullname;
        this.registeredAt = Date.now();
        this.avaUrl = avaUrl;
        this.isDisabled = isDisabled;
        this.bio = bio;
        this.dateOfBirth = dateOfBirth;
    }

    static getAll(){
        return UserModel.find();
    }
    static getById(id){
        return UserModel.findById(id);
    }

};

//module.exports = User;
module.exports = User;


/*
function User(name){
    this.name = name;
    this.printName = function(){
        console.log(this.name);
    }
}
*/
//const artem = new User("artem");
//artem.printName();