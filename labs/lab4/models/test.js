'use strict';

const fs = require('fs');

class Test{

    constructor (name,questions, picUrl, authorID,subject,rating,numOfpeopleWhoPassed,numOfQuestions) {
        this.id = -1;
        this.name = name;
        this.questions = questions;
        this.picUrl = picUrl;
        this.authorID= authorID;
        let today = new Date(Date.now());
        this.created = today.toISOString();
        this.rating = rating;
        this.subject = subject;
        this.numOfpeopleWhoPassed = numOfpeopleWhoPassed;
        this.numOfQuestions = numOfQuestions;
    }

    static getAll(callback){
        fs.readFile("/home/artem/projects/webprogbase/labs/lab4/data/tests.json", (err, data) => {
            if (err) {
                callback(err);
            }
            else{
                try {
                    let pack = JSON.parse(data.toString());
                    callback(null,pack.items);
                }catch (err) {
                    callback(err);
                }

            }
        }); 
    }

    static getSize(pack){
        let counter = 0;
        for(let temp of pack){
            counter++;
        }
        return counter;
    }
    static cheakErrors(pack){
        if (pack === null || pack.items === null || pack.nextId === null || pack === false){
            return false;
        }
        return true;
    }
    
    static delete(id,callback){
        fs.readFile("/home/artem/projects/webprogbase/labs/lab4/data/tests.json", (err, data) => {
            if(err){
                callback(err);
            }
            else{
                try {
                    let pack = JSON.parse(data.toString());
                    let flag = false;
                    let counter = 0;
                    
                    for(let temp of pack.items){
                        if(temp!==null &&temp.id!==null && temp.id === id){
                            flag = true;
                            break;
                        }
                        counter++;
                    }   
            
                    if(flag){
                        pack.items.splice(counter, 1);
                        let strToSave = JSON.stringify(pack,null,4);
                        try{
                            fs.writeFile("/home/artem/projects/webprogbase/labs/lab4/data/tests.json",strToSave,(err)=>{
                                if(err){
                                    callback(err);
                                }else{
                                    callback(null,"done");
                                }
                            });                                 
                        }
                        catch(err){
                            callback(err);
                        }
                    }
                    else callback(new Error("not Found"));
                    
                } catch (err) {
                    callback(err);
                }        
            }
        }
        );
        
    }
    static update(toUpd){
        let pack = this.parseFile();
        
        if(!this.cheakErrors(pack)){
            return false;
        }

        let counter = 0;
        for(let temp of pack.items){
            if(temp!==null && temp.id!==null && temp.id === toUpd.id){
                pack.items[counter] = toUpd;
                this.rewriteFile(pack);
                return true;            
            }
            counter++;
        }    
        return false;
    
    }
    static insert(toSave,callback){
        if(toSave.name!==null ){
                let strToSave = "";
            
                fs.readFile("/home/artem/projects/webprogbase/labs/lab4/data/tests.json",(err,data)=>{
                    if(err){
                        toSave.id = 0;
                        const pack = {
                            nextId:1,
                            items:[toSave]
                        };
                        strToSave = JSON.stringify(pack,null,4);
                    }else{
                        let pack;
                            try{
                                pack = JSON.parse(data.toString());
                                toSave.id = pack.nextId;
                                pack.items[pack.items.length] = toSave;
                                pack.nextId += 1;
                                strToSave = JSON.stringify(pack,null,4);             
                            }
                            catch(err){
                                callback(err);
                            }                             
                    }
                    if(strToSave!=""){
                        fs.writeFile("/home/artem/projects/webprogbase/labs/lab4/data/tests.json",strToSave,(err)=>{
                                if(err){
                                    callback(err);
                                }else{
                                    callback(null,"done");
                                }
                        });                                         
                    }            
                });  
        }else callback(new Error("Error because of empty entity"));
                
    }
    static getById(id,callback){
        this.getAll((err,data)=>{
              
            if (err){
                callback(err);
            }

            let flag = false;

            for(let temp of data){
                if(temp!==null && temp.id!==null && temp.id === id){
                    callback(null,temp);
                    flag = true;
                }
            }
            if(!flag){
                callback(new Error("Element not found"));
            }   
        });
    }

    static printByid(id){
        console.log (this.getById(id));
    }

    static parseFile(callback){
        const content = fs.readFileSync("/home/artem/projects/webprogbase/labs/lab4/data/tests.json");   
        try {
            let pack = JSON.parse(content.toString());
            return pack;
        } catch (err) {
            return false;
        }     
    }
    static rewriteFile(pack){
        let strToSave = JSON.stringify(pack,null,4);
        try{
            fs.writeFileSync("/home/artem/projects/webprogbase/labs/lab4/data/tests.json",strToSave);                                 
        }
        catch(err){
            throw err;
        }
     }
}

//let x = new test.create(0,"test1",["1","2jjjj","3"],"www.aaaa","Artem Trush",0,"test");

//let z = new test.create(1,"test2",["1","2jjjj","3"],"wwfafa","Trush",0,"test2");

//let y = new test.create(0,"t",["1","2jjjj","3"],"www.","Artem",0,"tt");

//test.insert(x);

//test.insert(z);

//test.delete(0);

module.exports = Test;