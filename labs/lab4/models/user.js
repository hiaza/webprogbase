'use strict';
const fs = require('fs');

class User{
    static getAll(callback){
        fs.readFile("/home/artem/projects/webprogbase/labs/lab4/data/user.json", (err, data) => {
            if (err) {
                callback(err);
            }
            else{
                try {
                    let pack = JSON.parse(data.toString());
                    callback(null,pack.items);
                }catch (err) {
                    callback(err);
                }

            }
        }); 
    }
    static getById(id,callback){
        this.getAll((err,pack)=>{
            if(err){
                callback(err);
            }
            else{
                let flag = false;
            
                for(let temp of pack){
                    if(temp!==null && temp.id!==null && temp.id === id){
                        callback(null,temp);
                        flag = true;
                    }
                }
                if(!flag){
                    callback(new Error("Author not found"));
                }
            }
        });
    }
    constructor (id, login, role, fullname, avaUrl, isDisabled,bio,dateOfBirth) {
        this.id = id;
        this.login = login;
        this.role = role;
        this.fullname = fullname;
        this.registeredAt = Date.now();
        this.avaUrl = avaUrl;
        this.isDisabled = isDisabled;
        this.bio = bio;
        this.dateOfBirth = dateOfBirth;
    }
};

//module.exports = User;
module.exports = User;


/*
function User(name){
    this.name = name;
    this.printName = function(){
        console.log(this.name);
    }
}
*/
//const artem = new User("artem");
//artem.printName();