const express = require('express');
const path = require('path');
const user_c = require('./models/user');
const test_c = require('./models/test');
const fs = require("fs");

const bodyParser = require('body-parser');
const mustache = require('mustache-express');
const busboyBodyParser = require('busboy-body-parser');

const viewsDir = path.join(__dirname, 'views');

const app = express();
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');


// usage

// view engine setup
/*
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
*/
/*
app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());*/
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.use(busboyBodyParser({ limit: '5mb' }));

app.use(express.static(path.join(__dirname, 'data/fs')));

app.get('/', function(req, res) {
    res.render('index', {  });
});
app.get('/about', function(req, res) {
    res.render('about', {  });
});

app.get('/tests/new', function(req, res) {
    res.render('test_creation', {  });
});

app.get('/users/:id(\\d+)',function(req, res,next) {
    console.log(`got some request: ${req.params.id}`);
    user_c.getById(parseInt(req.params.id),(err,choosed_user)=>{
        
        if(err)res.status(404).send("Error 404");
        
        else{
        
            const usersData = {
                "bio":choosed_user.bio,
                "fullname":choosed_user.fullname,
                "login":choosed_user.login,
                "avaUrl":choosed_user.avaUrl,
                "dateOfBirth":choosed_user.dateOfBirth
            };    
    
            res.setHeader('Content-Type', 'text/html');
            res.render('user', usersData);
    
        }
    });
});

app.get('/users',function(req, res,next) {
    console.log(`got some request`);
    user_c.getAll((err,users_all)=>{
      
        if(err) res.status(404).send("Error 404");
        
        else{
            const usersData = {users_all};
            res.setHeader('Content-Type', 'text/html');    
            res.render('users', usersData);
        }

    });
});

app.get('/api/users/:id(\\d+)',function(req, res,next) {
    
    user_c.getById(parseInt(req.params.id),(err,choosed_user)=>{
        if(err) res.status(404).send("Error 404");
        else {
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(choosed_user,null,4));    
        }
    });

});

app.get('/api/users',function(req, res,next) {
    
    user_c.getAll((err,users_all)=>{
        if(err) res.status(404).send("Error 404");
        else{
            const resultString = JSON.stringify(users_all,null,4);
            res.setHeader('Content-Type', 'application/json');
            res.end(resultString);
        }
    });

});

app.post('/delete/:id',function(req, res) {
    const id = parseInt(req.params.id);
    test_c.delete(id,(err,mes)=>{
        if(err){
            res.status(404).send(err.message);              
        }else if (mes) res.redirect("../tests");
    }); 
});

app.get('/tests/:id(\\d+)',function(req, res) {

    test_c.getById(parseInt(req.params.id),(err,choosed_test)=>{
        if(err){
            res.status(404).send("Error 404");
        }
        else{
            user_c.getById(choosed_test.authorID,(err,author)=>{
                if(err)res.status(404).send("Error 404");
                
                else{
                    const testData = {
                        "id": choosed_test.id,
                        "name": choosed_test.name,
                        "picUrl": choosed_test.picUrl,
                        "author": author.login,
                        "authorUrl": "../users/"+ choosed_test.authorID,
                        "created": choosed_test.created,
                        "rating": choosed_test.rating,
                        "subject": choosed_test.subject,
                        "numOfpeopleWhoPassed": choosed_test.numOfpeopleWhoPassed,
                        "numOfQuestions": choosed_test.numOfQuestions
                    };    
                    res.setHeader('Content-Type', 'text/html');
                    res.render('test', testData);
                }

            });
            
       }
    });
});

app.post('/tests/new',function(req, res) {
    
        let test_pic = req.files.test_pic;        
        let newPath = path.join(__dirname, '/data/fs/'+ test_pic.name);
        console.log(newPath);
        user_c.getById(parseInt(req.body.test_author_id),(err,author)=>{
            if(err)  res.status(404).send(err.message);
            else{

                let temp = new test_c(req.body.test_name,
                    "...",
                    '../files/' + test_pic.name,
                    author.id,
                    req.body.test_subject,
                    parseFloat(req.body.test_rating),
                    0,10
                );
        
                test_c.insert(temp,(err,mes)=>{
                    if(err){
                        res.status(404).send(err.message);
                          
                    }else{
                        if(mes){
                            fs.writeFile(newPath,test_pic.data,(err)=>{
                                if(err){
                                    res.status(404).send(err.message);
                                }else{
                                   console.log(temp.id);
                                   res.redirect("../tests/"+temp.id);
                                }
                             });            
                        }
                    } 
                });
                console.log(temp);
            }
            
        });
        
});

app.get("/files/:name",function(req, res) {
            
        let Path = path.join(__dirname, '/data/fs/'+ req.params.name);
        fs.readFile(Path,(err,pic)=>{
            if(err) res.status(404).send(err.message);
            else{
                res.send(pic);
            }
        });
        
});

/* 40 мін автомагістраль  50 макс буксіровка і шось там
   60 макс грузові з людьми ,мопед
   70 зі стажем від 2 років
   80 автобуси які організовані групи дітей перевозять легкова машина з причепом мотоцикли
   90 автобуси дорога вне населенного пункту на проїжджій частині
   130 макс для автомагістралі
*/
    // const choosed_test = test_c.getById(parseInt(req.params.id));

    // if(choosed_test!==false){

    //     const testData = {
    //         "id": choosed_test.id,
    //         "name": choosed_test.name,
    //         "picUrl": choosed_test.picUrl,
    //         "author": choosed_test.author,
    //         "authorUrl":choosed_test.authorUrl,
    //         "created": choosed_test.created,
    //         "rating": choosed_test.rating,
    //         "subject": choosed_test.subject,
    //         "numOfpeopleWhoPassed": choosed_test.numOfpeopleWhoPassed,
    //         "numOfQuestions": choosed_test.numOfQuestions
    //     };    
    //     res.setHeader('Content-Type', 'text/html');
    //     res.render('test', testData);
    
    // }else{
    //     res.status(404).send("Error 404");;  
    // }



/*app.get('/tests',function(req, res,next) {
    const tests_all = test_c.getAll();
    if(tests_all!==false){
        const testsData = {tests_all};
        res.setHeader('Content-Type', 'text/html');
        res.render('tests', testsData);
    }else{
        res.status(404).send("Error 404");  
    }

});
*/

function getSize(pack){
    let counter = 0;
    for(let temp of pack){
        counter++;
    }
    return counter;
}


function pagination (res,req,nameOfEnteties,data,EntetiesOnPage){
    let curPage = 0;
    let Next =  "visibility: visible";
    let Prev =  "visibility: hidden";
   
    if(req.query.page){
        curPage = parseInt(req.query.page);
        if (curPage != 0){
            Prev = "visibility: visible";
        }
    }        
    let isSearch = false;
    let searchData = [];
    if(req.query.search){
        isSearch = true;
        let counter = 0;
        for(let temp of data){
            if(temp.name!== undefined){
                if (~temp.name.indexOf(req.query.search)) {
                    searchData[counter] = temp;
                    counter++;
                }
            }else if(temp.question!== undefined){
                if (~temp.question.indexOf(req.query.search)) {
                    searchData[counter] = temp;
                    counter++;
                }
            }
        }
    }

    let pageCount;  
    if(isSearch){
        pageCount = getSize(searchData);
    }else{
        pageCount = getSize(data);     
    }
    
    let err = "";
    if(pageCount == 0 && isSearch){
        err = "Not found";
    }else if(pageCount == 0){
        err = "Empty list";
    }
    if(pageCount >=curPage*EntetiesOnPage){
    
            let arr;
            
                if(pageCount <= (curPage+1)*EntetiesOnPage){
                      
                    if(!isSearch){
                        arr = data.slice(curPage * EntetiesOnPage, pageCount);  
                    }
                      
                    else{
                        arr = searchData.slice(curPage * EntetiesOnPage, pageCount);
                    }      
                    Next = "visibility: hidden";
                  
                }else{

                    if(!isSearch){
                        arr = data.slice(curPage * EntetiesOnPage, (curPage+1)*EntetiesOnPage);
                      }
                    else{
                        arr = searchData.slice(curPage * EntetiesOnPage, (curPage+1)*EntetiesOnPage);
                      }     
                }           
            let findField = "";
            let findParam = "";
            if(isSearch){

                findField = req.query.search;
                findParam = "&search="+findField;
            
            }
            let pageAmont = parseInt(((pageCount*0.1) /(EntetiesOnPage*0.1))+0.8);
            
            if(pageAmont == 0) pageAmont = 1;

            const Data = {
                "tests_all": arr,
                "prev": Prev,
                "next": Next,
                "nextPage":curPage+1,
                "prevPage":curPage-1,
                "curPage":curPage+1,
                "pageCount":pageAmont,
                "findField":findField,
                "findParam":findParam,
                "err":err
            };

            res.setHeader('Content-Type', 'text/html');
            res.render(nameOfEnteties, Data);
            //res.send("yep");
    }
    else{
        res.status(404).send("Error");  
    }

}


app.get('/tests',function(req, res,next) {

    test_c.getAll((err, data) =>{
        if(err){
            res.status(404).send("Error 404");
        }
        else { 
            pagination(res,req,"tests",data,5);
            // let curPage = 0;
            // let Next =  "visibility: visible";
            // let Prev =  "visibility: hidden";
        
            // if(req.query.page){
            //     curPage = parseInt(req.query.page);
            //     if (curPage != 0){
            //         Prev = "visibility: visible";
            //     }
            // }        
            // let isSearch = false;
            // let searchData = [];
            // if(req.query.search){
            //     isSearch = true;
            //     let counter = 0;
            //     for(let temp of data){

            //         if (~temp.name.indexOf(req.query.search)) {
            //             searchData[counter] = temp;
            //             counter++;
            //         }
            //     }
            // }

            // let pageCount;
            // if(isSearch){
            //     pageCount = test_c.getSize(searchData);
            // }else{
            //     pageCount = test_c.getSize(data);     
            // }
            // let err = "";
            // if(pageCount == 0 && isSearch){
            //     err = "Not found";
            // }else if(pageCount == 0){
            //     err = "Empty list";
            // }
            // if(pageCount >=curPage*5){
            
            //         let arr;
                    
            //             if(pageCount <= (curPage+1)*5){
                              
            //                 if(!isSearch){
            //                     arr = data.slice(curPage * 5, pageCount);  
            //                 }
                              
            //                 else{
            //                     arr = searchData.slice(curPage * 5, pageCount);
            //                 }      
            //                 Next = "visibility: hidden";
                          
            //             }else{

            //                 if(!isSearch){
            //                     arr = data.slice(curPage * 5, (curPage+1)*5);
            //                   }
            //                 else{
            //                     arr = searchData.slice(curPage * 5, (curPage+1)*5);
            //                   }     
            //             }           
            //         let findField = "";
            //         let findParam = "";
            //         if(isSearch){

            //             findField = req.query.search;
            //             findParam = "&search="+findField;
                    
            //         }
            //         let pageAmont = parseInt((pageCount/5.0)+0.8);
                    
            //         if(pageAmont == 0) pageAmont = 1;

            //         const testsData = {
            //             "tests_all": arr,
            //             "prev": Prev,
            //             "next": Next,
            //             "nextPage":curPage+1,
            //             "prevPage":curPage-1,
            //             "curPage":curPage+1,
            //             "pageCount":pageAmont,
            //             "findField":findField,
            //             "findParam":findParam,
            //             "err":err
            //         };

            //         res.setHeader('Content-Type', 'text/html');
            //         res.render('tests', testsData);
            
            // }
            // else{
            //     res.status(404).send("Error 404");  
            // }
        }
    });

   

});


app.get('/api/tests/:id(\\d+)',function(req, res,next) {
    // const choosed_test = test_c.getById(parseInt(req.params.id));
    // if(choosed_test!==false){
    //     res.setHeader('Content-Type', 'application/json');
    //     res.end(JSON.stringify(choosed_test,null,4));
    // }else{
    //     res.status(404).send("Error 404");;  
    // }
    test_c.getById(parseInt(req.params.id),(err,choosed_test)=>{
        if(err){
            res.status(404).send("Error 404");
        }
        else{
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(choosed_test,null,4));
        }
    });
});

app.get('/api/tests',function(req, res,next) {
    test_c.getAll((err, data) =>{
        if(err){
            res.status(404).send("Error 404");
        }else{
            const resultString = JSON.stringify(data,null,4);
            res.setHeader('Content-Type', 'application/json');
            res.end(resultString);    
        }
   });
});
/// catch 404 and forwarding to error handler
// app.use(function(req, res, next) {
//     let err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

/// error handlers

// development error handler
// will print stacktrace
// if (app.get('env') === 'development') {
//     app.use(function(err, req, res, next) {
//         res.status(err.status || 500);
//         res.render('error', {
//             message: err.message,
//             error: err
//         });
//     });
// }

// production error handler
// no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.render('error', {
//         message: err.message,
//         error: {}
//     });
// });

module.exports = app;
