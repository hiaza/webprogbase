const express = require('express');
const user_c = require('../models/user');
const test_c = require('../models/test');
const question_c = require('../models/question');
const router = express.Router();
const passport = require('passport');
const extra = require('../models/extra');

function checkAdmin(req, res, next) {
    if (!req.user) res.sendStatus(401); 
    else if (req.user.role !== 1) res.sendStatus(403); 
    else next(); 
}
/*
router.get('/users',checkAdmin,function(req, res,next) {
    user_c.getAll()
    .then((data) =>{
        const resultString = JSON.stringify(data,null,4);
        res.setHeader('Content-Type', 'application/json');
        res.end(resultString);    
    })
    .catch((err)=> res.status(404).send(err.message))
});

router.get('/users/:id',checkAdmin,function(req, res,next) {
    user_c.getById(req.params.id)
    .then((choosed_user)=>{
           res.setHeader('Content-Type', 'application/json');
           res.end(JSON.stringify(choosed_user,null,4));
       })
    .catch ((err)=> res.status(404).send("Error 404"))
});

router.get('/questions',checkAdmin,function(req, res,next) {
    question_c.getAll()
    .then((data) =>{
        const resultString = JSON.stringify(data,null,4);
        res.setHeader('Content-Type', 'application/json');
        res.end(resultString);    
    })
    .catch((err)=> res.status(404).send(err.message))
});

router.get('/questions/:id',checkAdmin,function(req, res,next) {
    question_c.getById(req.params.id)
    .then((choosed_question)=>{
           res.setHeader('Content-Type', 'application/json');
           res.end(JSON.stringify(choosed_question,null,4));
       })
    .catch ((err)=> res.status(404).send("Error 404"))
});
    
router.get('/tests',checkAdmin,function(req, res,next) {
    test_c.getAll()
        .then((data) =>{
            const resultString = JSON.stringify(data,null,4);
            res.setHeader('Content-Type', 'application/json');
            res.end(resultString);    
        })
        .catch((err)=> res.status(404).send(err.message))
});

router.get('/tests/:id',checkAdmin,function(req, res,next) {
         test_c.getById(req.params.id)
         .then((choosed_test)=>{
                res.setHeader('Content-Type', 'application/json');
                res.end(JSON.stringify(choosed_test,null,4));
            })
         .catch ((err)=> res.status(404).send("Error 404"))
})
*/
router.get('/v1',function(req, res,next) {
    let simpleObj;
    //res.setHeader('Content-Type', 'application/json');
    //res.end(JSON.stringify(simpleObj,null,4));
    res.json(simpleObj);
})

function checkUser (req,res,next){
    passport.authenticate('basic', { session: false}, 
    function(err, user, info) {
        if (err) { return res.json(err.message); }
        if (!user) { return res.json({"error":"Invalid username or password."})} 
        req.logIn(user, function(err) {
          if (err) { return res.json({"error":err.message}); }
          next();
        });
    })(req, res);
}

router.get('/v1/me',checkUser, function(req, res){
        res.json(req.user);
 });

router.get('/v1/users',checkUser,function(req, res,next) {
    if(req.user.role != 1) return res.status(403).json({"error":"forbidden"}); 
    user_c.getAll()
        .then((data) =>{
            let EntetiesOnPage = 5;
            let curPage = 0;
            if(req.query.offset) {
                EntetiesOnPage = parseInt(req.query.offset);
                if(EntetiesOnPage<1) return res.json({"error":"Offset has to be more then 0"});
            }
            if(req.query.page) curPage = parseInt(req.query.page);
            let isSearch = false;
            let searchData = [];
            if(req.query.fullname && req.query.login) res.json({"error":"one field has to be used for search"})
            else if(req.query.fullname || req.query.login ){
                    isSearch = true;
                    let counter = 0;
                    for(let temp of data){
                        if(req.query.fullname){
                            if (~temp.fullname.indexOf(req.query.fullname)) {
                                searchData[counter] = temp;
                                counter++;
                            }
                        }else if(req.query.login){
                            if (~temp.login.indexOf(req.query.login)) {
                                searchData[counter] = temp;
                                counter++;
                            }
                        }
                    }
            }
            let pageCount;  

            if(isSearch) pageCount = extra.getSize(searchData);
            else pageCount = extra.getSize(data);     
            
            
            if(pageCount == 0 && isSearch){
                return res.json({"error": "Not found"})                        
            }else if(pageCount == 0){
                return res.json({"error": "Empty list"})                            
            }
            if(pageCount >=curPage*EntetiesOnPage){
                    let arr;
                    if(pageCount <= (curPage+1)*EntetiesOnPage){         
                        if(!isSearch) arr = data.slice(curPage * EntetiesOnPage, pageCount);    
                        else arr = searchData.slice(curPage * EntetiesOnPage, pageCount);  
                    }else{
                        if(!isSearch) arr = data.slice(curPage * EntetiesOnPage, (curPage+1)*EntetiesOnPage);                     
                        else arr = searchData.slice(curPage * EntetiesOnPage, (curPage+1)*EntetiesOnPage);        
                    }           
                    return res.json(arr);   
            }
            else{
                res.status(404).json({"Error":"Error with pagination"});  
            }
        })
        .catch((err)=> res.json({"Error": err.message}))
});

router.get('/v1/users/:id',checkUser,function(req, res,next) {
    if(req.user.role != 1) return res.status(403).json({"error":"forbidden"});
    user_c.getById(req.params.id)
    .then((choosed_user)=>{
        res.json(choosed_user);
    })
    .catch ((err)=> res.status(404).send("Error 404"))
});

router.get('/v1/questions',checkUser,function(req, res,next) {
    question_c.getAll()
    .then((pack) =>{
        let data = [];
        if(req.user.role!=1){
            for(let each of pack){
                if(each.authorId.toString() == req.user.id) data.push(each);
            }
        }else data = pack;

        let EntetiesOnPage = 5;
        let curPage = 0;
        if(req.query.offset) {
            EntetiesOnPage = parseInt(req.query.offset);
            if(EntetiesOnPage<1) return res.json({"error":"Offset has to be more then 0"});
        }
        if(req.query.page) curPage = parseInt(req.query.page);
        let isSearch = false;
        let searchData = [];
        if(req.query.question && req.query.rating) res.json({"error":"one field has to be used for search"})
        else if(req.query.question || req.query.rating ){
                isSearch = true;
                let counter = 0;
                for(let temp of data){
                    if(req.query.question){
                        if (~temp.question.indexOf(req.query.question)) {
                            searchData[counter] = temp;
                            counter++;
                        }
                    }else if(req.query.rating){
                        let rate = parseInt(req.query.rating); 
                        if(rate<0||rate>10) return res.json({"error":"Rating has to be from 0 to 10"});
                        if(temp.rating == rate) {
                                searchData[counter] = temp;
                                counter++;
                            
                        }
                    }
                }
        }
        let pageCount;  

        if(isSearch) pageCount = extra.getSize(searchData);
        else pageCount = extra.getSize(data);     
        
        
        if(pageCount == 0 && isSearch){
            return res.json({"error": "Not found"})                        
        }else if(pageCount == 0){
            return res.json({"error": "Empty list"})                            
        }
        if(pageCount >=curPage*EntetiesOnPage){
                let arr;
                if(pageCount <= (curPage+1)*EntetiesOnPage){         
                    if(!isSearch) arr = data.slice(curPage * EntetiesOnPage, pageCount);    
                    else arr = searchData.slice(curPage * EntetiesOnPage, pageCount);  
                }else{
                    if(!isSearch) arr = data.slice(curPage * EntetiesOnPage, (curPage+1)*EntetiesOnPage);                     
                    else arr = searchData.slice(curPage * EntetiesOnPage, (curPage+1)*EntetiesOnPage);        
                }           
                return res.json(arr);   
        }
        else{
            res.status(404).json({"Error":"Error with pagination"});  
        }
    })
    .catch((err)=> res.json({"Error": err.message}))
});


router.get('/v1/questions/:id',checkUser,function(req, res,next) {
    question_c.getById(req.params.id)
    .then((choosed_question)=>{
        if(req.user.role!=1 && choosed_question.authorId.toString()!=req.user._id){
            return res.json({"error":"Not your question"});
        }
        if(choosed_question!=null)res.json(choosed_question);
        else res.status(404).json({"error":"not found"})
    })
    .catch ((err)=> res.status(404).json({"error":"not found"}) )
});
   



router.get('/v1/tests',checkUser,function(req, res,next) {
    test_c.getAll()
        .then((pack) =>{
            let data = [];
            if(req.user.role!=1){
                for(let each of pack){
                    if(each.authorID.toString() == req.user.id) data.push(each);
                }
            }else data = pack;

            let EntetiesOnPage = 5;
            let curPage = 0;
            if(req.query.offset) {
                EntetiesOnPage = parseInt(req.query.offset);
                if(EntetiesOnPage<1) return res.json({"error":"Offset has to be more then 0"});
            }
            if(req.query.page) curPage = parseInt(req.query.page);
            let isSearch = false;
            let searchData = [];
            if(req.query.name && req.query.rating) res.json({"error":"one field has to be used for search"})
            else if(req.query.name || req.query.rating ){
                    isSearch = true;
                    let counter = 0;
                    for(let temp of data){
                        if(req.query.name){
                            if (~temp.name.indexOf(req.query.name)) {
                                searchData[counter] = temp;
                                counter++;
                            }
                        }else if(req.query.rating){
                            let rate = parseInt(req.query.rating); 
                            if(temp.rating == rate) {
                                    searchData[counter] = temp;
                                    counter++;
                                
                            }
                        }
                    }
            }
            
            let pageCount;  
            if(isSearch) pageCount = extra.getSize(searchData);
            else pageCount = extra.getSize(data);     
            if(pageCount == 0 && isSearch){
                return res.json({"error": "Not found"})                        
            }else if(pageCount == 0){
                return res.json({"error": "Empty list"})                            
            }
            if(pageCount >=curPage*EntetiesOnPage){
                    let arr;
                    if(pageCount <= (curPage+1)*EntetiesOnPage){         
                        if(!isSearch) arr = data.slice(curPage * EntetiesOnPage, pageCount);    
                        else arr = searchData.slice(curPage * EntetiesOnPage, pageCount);  
                    }else{
                        if(!isSearch) arr = data.slice(curPage * EntetiesOnPage, (curPage+1)*EntetiesOnPage);                     
                        else arr = searchData.slice(curPage * EntetiesOnPage, (curPage+1)*EntetiesOnPage);        
                    }           
                    return res.json(arr);   
            }
            else{
                res.status(404).json({"Error":"Error with pagination"});  
            }
        })
        .catch((err)=> res.json({"Error": err.message}))
    
});

router.get('/v1/tests/:id',checkUser,function(req, res,next) {

    test_c.getById(req.params.id)
         .then((choosed_test)=>{ if(choosed_test!=null)res.json(choosed_test);
            else res.status(404).json({"error":"not found"})
      })
        .catch ((err)=> res.status(404).json({"error":"not found"}))
})

router.post('/v1/questions',checkUser,function(req, res,next) { 
    if( req.query.question              && req.query.rating        &&
        req.query.points                && req.query.trueAnswer    &&
        req.query.fakeAnswer1           && req.query.fakeAnswer2   &&
        req.query.fakeAnswer3           && req.query.points>=0     && 
        req.query.points<6              && req.query.rating>=0     && 
        req.query.rating<10){
            let url = "http://res.cloudinary.com/drmonx1pj/raw/upload/v1542241568/pheqo5ctrnrzhsbgz7g2";
            if(req.query.url) url=req.query.url;
            let objQuest = {
                "type":             1,
                "question":         req.query.question,
                "picUrl":           url,
                "authorId":         req.user._id.toString(),
                "rating":           parseInt(req.query.rating),
                "points":           parseInt(req.query.points),
                "trueAnswer":       req.query.trueAnswer,
                "fakeAnswer1":      req.query.fakeAnswer1,
                "fakeAnswer2":      req.query.fakeAnswer2,
                "fakeAnswer3":      req.query.trueAnswer,
                "fakeAnswer4":      req.query.fakeAnswer3
            }
            let temp = new question_c(objQuest);
            question_c.insert(temp)
                    .then(quest=>{res.status(201).json(quest);})
                    .catch((err)=>res.send(err)) 
        }
        else{
            res.json({"error":"Check all arguments: question, rating, points, fakeAnswer1, fakeAnswer2, fakeAnswer3, trueAnswer"});
        }
});

router.post('/v1/tests',checkUser,function(req, res,next) { 
    if( req.query.name                  && req.query.rating        &&
        req.query.questions             && req.query.subject       &&
        req.query.rating>=0             &&req.query.rating<10       
    ){
            let pic = "http://res.cloudinary.com/drmonx1pj/raw/upload/v1542241568/pheqo5ctrnrzhsbgz7g2";
            if(req.query.pic) pic=req.query.pic;
                
                let questions = [];
                let numOfQuestions = 0;
                if(req.query.questions instanceof Array){
                    for(let question of req.query.questions){
                            questions[numOfQuestions] = question;
                            numOfQuestions++;
                    }
                }
                else {
                    questions[0] = req.query.questions;    
                    numOfQuestions++;
                }
                let temp = new test_c(      req.query.name,
                    questions,              pic,
                    req.user._id,           req.query.subject,  
                    parseInt(req.query.rating),       0,                   
                    numOfQuestions
                );
                test_c.insert(temp)
                    .then((tempp)=>{res.status(201).json(tempp);})
                    .catch((err)=>res.status(404).json({"error": err.message}))     
            }
        else{
            res.json({"error":"Check all arguments: name, questions, subject, rating"});
        }
});


router.put('/v1/tests/:id',checkUser, function(req, res) {
    const id = req.params.id;
    if(req.query.name || req.query.rating || req.query.subject){
        test_c.getById(id)
        .then((choosed_test)=>{
            if(choosed_test.authorID.toString() != req.user._id && req.user.role !=1){
                  return res.json({"error":"Not your test"});
              }
              let name = choosed_test.name;
              let rating = choosed_test.rating;
              let subject = choosed_test.subject;

              if(req.query.name)name = req.query.name;
              if(req.query.rating){
                if(req.query.rating<0||req.query.rating>10) return res.json({"error":"Check rating (from 1 to 10)"}); 
                rating = parseInt(req.query.rating);
            }             
              if(req.query.subject)subject = req.query.subject;

              let objQuest = {
                  "id":                   id,  
                  "name":                 name,
                  "rating":               rating,
                  "subject":              subject
              }
              test_c.update(objQuest)
                        .then((id)=>{
                            res.redirect("/api/v1/tests/" + id);})
                        .catch((err)=>res.status(404).json({"error": err.message}))
        })
        .catch((err)=>res.status(404).json({"error": err.message}))
    }
    else res.json({"error": "Filds for updating: name,rating,subject"})
});

router.put('/v1/questions/:id',checkUser, function(req, res) {
    const id = req.params.id;
    if(req.query.question    || req.query.points      || req.query.trueAnswer
    || req.query.fakeAnswer1 || req.query.fakeAnswer2 || req.query.fakeAnswer3 ){
        question_c.getById(id)
        .then((choosed_question)=>{
            
            if(choosed_question.authorId.toString() != req.user._id && req.user.role !=1){
                  return res.json({"error":"Not your test"});
              }
              let question = choosed_question.question;
              let points = choosed_question.points;
              let trueAnswer = choosed_question.trueAnswer;
              let fakeAnswer1 = choosed_question.fakeAnswer1;
              let fakeAnswer2 = choosed_question.fakeAnswer2;
              let fakeAnswer3 = choosed_question.fakeAnswer3;

              if(req.query.question)question = req.query.question;
              if(req.query.points){
                if(req.query.points<0 || req.query.points>6) return res.json({"error":"Check points (from 0 to 5)"});  
                points = parseInt(req.query.points);
              }                  
              if(req.query.trueAnswer)trueAnswer = req.query.trueAnswer;
              if(req.query.fakeAnswer1)fakeAnswer1 = req.query.fakeAnswer1;
              if(req.query.fakeAnswer2)fakeAnswer2 = req.query.fakeAnswer2;
              if(req.query.fakeAnswer3)fakeAnswer3 = req.query.fakeAnswer3;
 
              let objQuest = {
                "id":               id,
                "question":         question,
                "points":           points,
                "trueAnswer":       trueAnswer,
                "fakeAnswer1":      fakeAnswer1,
                "fakeAnswer2":      fakeAnswer2,
                "fakeAnswer3":      fakeAnswer3,
                "fakeAnswer4":      trueAnswer
            }
    
            question_c.update(objQuest)
                .then((id)=>{
                    res.redirect("/api/v1/questions/" + id);})
                .catch((err)=>res.status(404).json({"error": err.message}))
        })
        .catch((err)=>res.status(404).json({"error": err.message}))
    }
    else res.json({"error": "Filds for updating: question, points,trueAnswer,fakeAnswer1,fakeAnswer2,fakeAnswer3"})
});

router.put('/v1/users/:id',checkUser, function(req, res) {
    const id = req.params.id;
    if(req.query.fullname || req.query.bio || req.query.avaUrl){
        user_c.getById(id)
        .then((choosed_user)=>{
            if(choosed_user._id.toString() != req.user._id && req.user.role !=1){
                  return res.json({"error":"Not your test"});
              }
              let fullname = choosed_user.fullname;
              let bio = choosed_user.bio;
              let avaUrl = choosed_user.avaUrl;
              if(req.query.fullname) {
                  fullname = req.query.fullname;
            }

              if(req.query.bio) bio = req.query.bio;
              if(req.query.avaUrl) avaUrl = req.query.avaUrl;
            
              let objQuest = {
                  "id":                   id,  
                  "fullname":             fullname,
                  "bio":                  bio,
                  "avaUrl":               avaUrl
              }

              user_c.update(objQuest)
                        .then((id)=>{
                            res.redirect("/api/v1/users/" + id);})
                        .catch((err)=>res.status(404).json({"error": err.message}))
        })
        .catch((err)=>res.status(404).json({"error": err.message}))
    }
    else res.json({"error": "Filds for updating: fullname,bio,avaUrl"});
});

router.delete('/v1/tests/:id',checkUser,function(req, res) {   
   const id = req.params.id;
   test_c.getById(id)
   .then((choosed_test)=>{   
    if(choosed_test.authorID.toString() != req.user._id && req.user.role !=1){
        return res.json({"error":"Not your test"});
    }     
    return test_c.delete(id)})
    .then(()=>{ res.redirect("/api/v1/tests");})
    .catch((err)=>res.status(404).json({"error": err.message}))

});

router.delete('/v1/questions/:id',checkUser,function(req, res) {   
    const id = req.params.id;
    question_c.getById(id)
    .then((choosed_question)=>{   
     if(choosed_question.authorId.toString() != req.user._id && req.user.role !=1){
         return res.json({"error":"Not your question"});
     }     
     return question_c.delete(id)})
     .then(()=>{ res.redirect("/api/v1/questions");})
     .catch((err)=>res.status(404).json({"error": err.message}))
 });

module.exports = router;