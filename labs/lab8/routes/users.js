const express = require('express');
const user_c = require('../models/user');

const extra = require('../models/extra');
const router = express.Router();


function checkAdmin(req, res, next) {
    if (!req.user) res.sendStatus(401); // 'Not authorized'
    else if (req.user.role !== 1) res.sendStatus(403); // 'Forbidden'
    else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
}


router.get('/',checkAdmin,function(req, res,next) {
    console.log(`got some request`);
    user_c.getAll()
          .then((users_all)=>{
           /* let admin;
            if(req.user.role==1){
                admin=1;
            }*/
            const usersData = {users_all,
                "user":req.user,
                "admin": extra.checkAdmin(req)};
            res.setHeader('Content-Type', 'text/html');    
            res.render('users', usersData);
          })
          .catch((err)=>res.status(404).send(err.message))
});

router.get('/:id',function(req, res,next) {
    console.log(`got some request: ${req.params.id}`);
    if(req.user.role!=1 && req.user._id!=req.params.id){
        res.sendStatus(403);
        return;
    }
    let notHisPage;
    if(req.user._id!=req.params.id) {
        notHisPage = 1;
    }
    user_c.getById(req.params.id)
        .then((choosed_user)=>{ 
            let dateOfBirth = choosed_user.dateOfBirth.toLocaleDateString();
            let isUser;
            let isAdmin;
            if(choosed_user.role == 0) isUser = "checked";
            else isAdmin = "checked";
                const usersData = {
                    "bio":choosed_user.bio,
                    "fullname":choosed_user.fullname,
                    "login":choosed_user.login,
                    "_id":choosed_user._id,
                    "avaUrl":choosed_user.avaUrl,
                    "dateOfBirth": dateOfBirth,
                    "user":req.user,
                    "admin": extra.checkAdmin(req),
                    "notHisPage": notHisPage,
                    "isUser": isUser,
                    "isAdmin": isAdmin
                };    
                res.setHeader('Content-Type', 'text/html');
                res.render('user', usersData);
        })
        .catch((err)=>res.status(404).send("Error 404"))  
});


router.post('/change_role/:id',function(req, res,next) {

    user_c.getById(req.params.id)
        .then((choosed_user)=>{ 
                choosed_user.role = parseInt(req.body.role);
                return user_c.update(choosed_user);      
        })
        .then((id)=>res.redirect("/users/"+id))
        .catch((err)=>res.status(404).send("Error 404"))  
});

module.exports = router;