const express = require('express');
const user_c = require('../models/user');
const test_c = require('../models/test');
const question_c = require('../models/question');
const config = require('../config');
const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});


const router = express.Router();


const extra = require('../models/extra');

router.get('/new', function(req, res) {
    user_c.getAll()
        .then((data)=>{
            const QData = {
                "all_authors":data,
                 "user": req.user,
                 "admin": extra.checkAdmin(req) 
            };
            res.render('question_creating',QData);
        })
        .catch((err)=> res.status(404).send("Error 404"))
});

router.post('/new',function(req, res) {
    let pic = req.files.quest_pic;        
    let newPath = '/home/artem/projects/webprogbase/labs/lab5/data/fs/'+pic.name;
    let authorID = req.user._id;
    const fileBuffer = pic.data;
    cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },
        function (error, result) { 
            //console.log(result, error) 
            if(error) res.send(err);
            else{
                let objQuest = {
                    "type":             1,
                    "question":         req.body.quest,
                    "picUrl":           result.url,
                    "authorId":         authorID,
                    "rating":           req.body.quest_rating,
                    "points":           req.body.quest_points,
                    "trueAnswer":       req.body.quest_trueAnswer,
                    "fakeAnswer1":      req.body.quest_fakeAnswer1,
                    "fakeAnswer2":      req.body.quest_fakeAnswer2,
                    "fakeAnswer3":      req.body.quest_trueAnswer,
                    "fakeAnswer4":      req.body.quest_fakeAnswer3
                }
        
                let temp = new question_c(objQuest);
                question_c.insert(temp)
                        .then(quest=>{res.redirect("../questions/" + quest._id);})
                        .catch((err)=>res.send(err))
                    }}) .end(fileBuffer)        
});
    
router.post('/update',function(req, res) {
        
        let objQuest = {
            "id":               req.body.id,
            "question":         req.body.question,
            "points":           req.body.points,
            "trueAnswer":       req.body.trueAnswer,
            "fakeAnswer1":      req.body.fakeAnswer1,
            "fakeAnswer2":      req.body.fakeAnswer2,
            "fakeAnswer3":      req.body.trueAnswer,
            "fakeAnswer4":      req.body.fakeAnswer3
        }

        question_c.update(objQuest)
                  .then((id)=>{
                      res.redirect("../questions/" + id);})
                  .catch((err)=>res.send(err))

        
});


router.get('/',function(req, res,next) {
   /*question_c.getAll()
    .then((data)=>{
        let pack = [];
        for(let each of data){
            if(each.authorId.toString() == req.user.id) pack.push(each);
        }
        extra.pagination(res,req,'questions',pack,5)
    })
    .catch((err)=> res.status(404).send(err.mes))*/
    res.setHeader('Content-Type', 'text/html');
    res.render("questions.mst",{ 
        "user":         req.user,
        "admin":        extra.checkAdmin(req)
    });
});

router.get('/:id',function(req, res) {
    
        question_c.getById(req.params.id)
                  .then((choosed_question)=>{  
                    return Promise.all([choosed_question,user_c.getById(choosed_question.authorId)])
                  })
                  .then(([choosed_question,author])=>{
                            const questionData = {
                            
                                "id":           choosed_question.id,
                                "type":         choosed_question.type,
                                "picUrl":       choosed_question.picUrl,
                                "question":     choosed_question.question,
                                "created":      choosed_question.created,
                                "rating":       choosed_question.rating,
                                "points":       choosed_question.points,
                                "trueAnswer":   choosed_question.trueAnswer,
                                "fakeAnswer1":  choosed_question.fakeAnswer1,
                                "fakeAnswer2":  choosed_question.fakeAnswer2,
                                "fakeAnswer3":  choosed_question.fakeAnswer3,
                                "fakeAnswer4":  choosed_question.fakeAnswer4,
                                "author":       author.login,
                                "authorUrl":    "../users/" + author.id,
                                "user":         req.user,
                                "admin":        extra.checkAdmin(req)
                            };    
                            res.setHeader('Content-Type', 'text/html');
                            res.render('question', questionData);
                })  
                .catch((err)=>res.status(404).send(err))
            
    });

    // auth middlewares
  
    
module.exports = router;