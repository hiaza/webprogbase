const express = require('express');
const path = require('path');
// const favicon = require('static-favicon');
// const logger = require('morgan');
// const cookieParser = require('cookie-parser');
// const bodyParser = require('body-parser');
// const consolidate = require('consolidate');
// const routes = require('./routes/index');
// const users = require('./routes/users');
// const user = require('./routes/user');
// const about = require('./routes/about');
// const tests = require('./routes/tests');
// const test = require('./routes/test');
const user_c = require('./models/user');
const test_c = require('./models/test');
//const fs = require("fs");

const mustache = require('mustache-express');

const app = express();
const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

// usage

// view engine setup
/*
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
*/
/*
app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());*/
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
    res.render('index', {  });
});
app.get('/about', function(req, res) {
    res.render('about', {  });
});

app.get('/users/:id(\\d+)',function(req, res,next) {
    console.log(`got some request: ${req.params.id}`);
    const choosed_user = user_c.getById(parseInt(req.params.id));
    if(choosed_user!==false){
        const usersData = {
            "bio":choosed_user.bio,
            "fullname":choosed_user.fullname,
            "login":choosed_user.login,
            "avaUrl":choosed_user.avaUrl,
            "dateOfBirth":choosed_user.dateOfBirth
        };    

        res.setHeader('Content-Type', 'text/html');
        res.render('user', usersData);

    }else{
        res.status(404).send("Error 404");;  
    }
});

app.get('/users',function(req, res,next) {
    console.log(`got some request`);
    const users_all = user_c.getAll();
    if(users_all!==false){
        const usersData = {users_all};
        res.setHeader('Content-Type', 'text/html');    
        res.render('users', usersData);
    }else{

        res.status(404).send("Error 404");  

    }
});

app.get('/api/users/:id(\\d+)',function(req, res,next) {
    const choosed_user = user_c.getById(parseInt(req.params.id));
    if(choosed_user!==false){
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(choosed_user,null,4));
    }else{
        res.status(404).send("Error 404");;  
    }

});

app.get('/api/users',function(req, res,next) {
    const users_all = user_c.getAll();
    if(users_all!==false){
        const resultString = JSON.stringify(users_all,null,4);
        res.setHeader('Content-Type', 'application/json');
        res.end(resultString);
    }else{
        res.status(404).send("Error 404");  

    }

});

app.get('/tests/:id(\\d+)',function(req, res,next) {

    const choosed_test = test_c.getById(parseInt(req.params.id));

    if(choosed_test!==false){

        const testData = {
            "id": choosed_test.id,
            "name": choosed_test.name,
            "picUrl": choosed_test.picUrl,
            "author": choosed_test.author,
            "authorUrl":choosed_test.authorUrl,
            "created": choosed_test.created,
            "rating": choosed_test.rating,
            "subject": choosed_test.subject,
            "numOfpeopleWhoPassed": choosed_test.numOfpeopleWhoPassed,
            "numOfQuestions": choosed_test.numOfQuestions
        };    
        res.setHeader('Content-Type', 'text/html');
        res.render('test', testData);
    
    }else{
        res.status(404).send("Error 404");;  
    }

});

app.get('/tests',function(req, res,next) {
    const tests_all = test_c.getAll();
    if(tests_all!==false){
        const testsData = {tests_all};
        res.setHeader('Content-Type', 'text/html');
        res.render('tests', testsData);
    }else{
        res.status(404).send("Error 404");  
    }

});

app.get('/api/tests/:id(\\d+)',function(req, res,next) {
    const choosed_test = test_c.getById(parseInt(req.params.id));
    if(choosed_test!==false){
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(choosed_test,null,4));
    }else{
        res.status(404).send("Error 404");;  
    }

});

app.get('/api/tests',function(req, res,next) {
    const tests_all = test_c.getAll();
    if(tests_all!==false){
        const resultString = JSON.stringify(tests_all,null,4);
        res.setHeader('Content-Type', 'application/json');
        res.end(resultString);
    }else{
        res.status(404).send("Error 404");  

    }
});
/// catch 404 and forwarding to error handler
// app.use(function(req, res, next) {
//     let err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

/// error handlers

// development error handler
// will print stacktrace
// if (app.get('env') === 'development') {
//     app.use(function(err, req, res, next) {
//         res.status(err.status || 500);
//         res.render('error', {
//             message: err.message,
//             error: err
//         });
//     });
// }

// production error handler
// no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.render('error', {
//         message: err.message,
//         error: {}
//     });
// });

module.exports = app;
