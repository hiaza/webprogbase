'use strict';

const fs = require('fs');

class Test{

    constructor (name,questions, picUrl, author,authorUrl,subject,rating,numOfpeopleWhoPassed,numOfQuestions) {
        const content = fs.readFileSync("/home/artem/projects/webprogbase/labs/lab3/data/tests.json");
        const pack = JSON.parse(content.toString());
        if(pack===false){
            this.id = 0;
        }else{
            this.id = pack.nextId;
        }
        this.name = name;
        this.questions = questions;
        this.picUrl = picUrl;
        this.author = author;
        this.authorUrl = authorUrl;
        let today = new Date(Date.now());
        this.created = today.toISOString();
        this.rating = rating;
        this.subject = subject;
        this.numOfpeopleWhoPassed = numOfpeopleWhoPassed;
        this.numOfQuestions = numOfQuestions;
    }

    static getAll(){
        const pack = this.parseFile();
        if(!this.cheakErrors(pack)){
            return false;
        }
        return pack.items; 
    }
    static cheakErrors(pack){
        if (pack === null || pack.items === null || pack.nextId === null || pack === false){
            return false;
        }
        return true;
    }
    static delete(id){
        let pack = this.parseFile();
        
        if(!this.cheakErrors(pack)){
            return false;
        }

        let flag = false;
        let counter = 0;
        
        for(let temp of pack.items){
            if(temp!==null &&temp.id!==null && temp.id === id){
                flag = true;
                break;
            }
            counter++;
        }   

        if(flag){
            pack.items.splice(counter, 1);
            this.rewriteFile(pack);                     
            return true;
        }
        return false;
    }
    static update(toUpd){
        let pack = this.parseFile();
        
        if(!this.cheakErrors(pack)){
            return false;
        }

        let counter = 0;
        for(let temp of pack.items){
            if(temp!==null && temp.id!==null && temp.id === toUpd.id){
                pack.items[counter] = toUpd;
                this.rewriteFile(pack);
                return true;            
            }
            counter++;
        }    
        return false;
    
    }
    static insert(toSave){
        if(toSave.name!==null ){
                let content; 
                let strToSave;
                try {        
                    content = fs.readFileSync("/home/artem/projects/webprogbase/labs/lab3/data/tests.json");  
                } catch (ENOENT) {
                    const pack = {
                        nextId:1,
                        items:[toSave]
                      };
                      strToSave = JSON.stringify(pack,null,4);
                 }
                 if(content !== null){
                    let pack = JSON.parse(content.toString());
                    pack.items[pack.items.length] = toSave;
                    pack.nextId += 1;
                    strToSave = JSON.stringify(pack,null,4);
                   } 

                   try{
                    fs.writeFileSync("/home/artem/projects/webprogbase/labs/lab3/data/tests.json",strToSave);                                         
                   }
                   catch(err){
                       return false;
                   }
                   return true;
        }
        return false;
    }
    static getById(id){
        const pack = this.getAll();
        
        if (pack === false){
            return false;
        }

        for(let temp of pack){
            if(temp!==null && temp.id!==null && temp.id === id){
                return temp; 
            }
        }    
        return false;
    }

    static printByid(id){
        console.log (this.getById(id));
    }

    static parseFile(){
        const content = fs.readFileSync("/home/artem/projects/webprogbase/labs/lab3/data/tests.json");   
        try {
            let pack = JSON.parse(content.toString());
            return pack;
        } catch (ELIFECYCLE) {
            return false;
        }     
    }
    static rewriteFile(pack){
        let strToSave = JSON.stringify(pack,null,4);
        try{
            fs.writeFileSync("/home/artem/projects/webprogbase/labs/lab3/data/tests.json",strToSave);                                 
        }
        catch(err){
            throw err;
        }
     }
}

//let x = new test.create(0,"test1",["1","2jjjj","3"],"www.aaaa","Artem Trush",0,"test");

//let z = new test.create(1,"test2",["1","2jjjj","3"],"wwfafa","Trush",0,"test2");

//let y = new test.create(0,"t",["1","2jjjj","3"],"www.","Artem",0,"tt");

//test.insert(x);

//test.insert(z);

//test.delete(0);

module.exports = Test;