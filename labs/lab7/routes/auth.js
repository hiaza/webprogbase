const express = require('express');
const user_c = require('../models/user');
const test_c = require('../models/test');
const question_c = require('../models/question');
const router = express.Router();
const passport = require('passport');
const config = require('../config');
const cloudinary = require('cloudinary');
const extra = require('../models/extra');


cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});


function loginIsCorrect(login){
    if(/^[a-zA-Z1-9]+$/.test(login) === false)
        return {status:false,error:"only+latin+letters+and+numbers"};
    if(login.length < 4 || login.length > 20)
        return {status:false,error:"login+length+must+be+more+then+4+and+less+then+20"};
    if(parseInt(login.substr(0, 1)))
        return {status:false,error:"login+has+to+begin+with+letter"};

     return {status:true,error:" "};
}
    
function passIsCorrect(textPass,textPass2){
    if(textPass != textPass2){
        return {status:false,error:"password1+not+equal+password2"};
    }
      var r=/[^A-Z-a-z-0-9]/g; 
      if(r.test(textPass)){
          return {status:false,error:"only+latin+letters+and+numbers"};
      }
      if (textPass.length<6){
          return {status:false,error:"password+is+too+short"};
      }
      if (textPass.length>20){
          return {status:false,error:"password+is+too+long"};
      }return {status:true,error:" "};
}

const serverSalt = "45%sAlT_";

router.get('/register',function(req, res,next) {
    res.render("registration", { user: req.user,error1:req.query.error1,error2:req.query.error2});
});

router.post('/register',function(req, res,next) {
    let loginFlag = loginIsCorrect(req.body.login);
    let passFlag = passIsCorrect(req.body.password1,req.body.password2);
    
    if(loginFlag.status && passFlag.status){
        user_c.checkLogin(req.body.login)
            .then(()=>{
                let pic = req.files.pic;        
                const fileBuffer = pic.data;
                cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },
                    function (error, result) { 
                        if(error) res.send(error);
                        else{
                            let login = req.body.login;                     
                            let temp = new user_c(login.toLowerCase(),
                            extra.getHash(req.body.password1).passwordHash,
                            0, req.body.first_n + " " + req.body.last_n,
                            result.url,false, req.body.bio,
                            req.body.bday);
                            user_c.insert(temp)
                                    .then(()=>{res.redirect("/auth/login")})
                                    .catch((err)=>res.send(err))
                            }}) .end(fileBuffer);
                
            })
            .catch((err)=>res.redirect("/auth/register?error1=User+exists&error2="+passFlag.error));        
        }
    else res.redirect("/auth/register?error1="+loginFlag.error+"&error2="+passFlag.error);
});

router.get('/login',function(req, res,next) {
    if(req.query.message){
        console.log(req.query.message);
        res.render("login",{'error':req.query.message});
    }
    else res.render("login");
});


router.post('/login',
passport.authenticate('local', { failureRedirect: '/auth/login?message=incorrect+login+or+password' }),  
(req, res) => res.redirect('/'));

router.get('/logout', 
(req, res) => {
    req.logout();   
    res.redirect('/');
});

module.exports = router;