'use strict';
const fs = require('fs');

class User{
    static getAll(){
        try{
            const content = fs.readFileSync("/home/artem/projects/webprogbase/labs/lab2/data/user.json");
            const pack = JSON.parse(content.toString());    
            return pack.items;
        }
        catch(err){
            return false;
        } 
    }
    static getById(id){
        const pack = this.getAll();
        
        if (pack === false){
            return false;
        }

        for(let temp of pack){
            if(temp!==null && temp.id!==null && temp.id === id){
                return temp; 
            }
        }    
        return false;
    }
    constructor (id, login, role, fullname, avaUrl, isDisabled) {
    this.id = id;
    this.login = login;
    this.role = role;
    this.fullname = fullname;
    this.registeredAt = Date.now();
    this.avaUrl = avaUrl;
    this.isDisabled = isDisabled;
    }
};

//module.exports = User;
module.exports = User;


/*
function User(name){
    this.name = name;
    this.printName = function(){
        console.log(this.name);
    }
}
*/
//const artem = new User("artem");
//artem.printName();