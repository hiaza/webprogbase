'use strict';

const user_functions = require('./models/user');
const test_functions = require('./models/test');
const {ServerApp, ConsoleBrowser, InputForm} = require("webprogbase-console-view");

const app = new ServerApp();
const browser = new ConsoleBrowser();


app.use("/",function(req,res){
    const message = "This is lab2!";
    const links = {
        "users":"Visit us",
        "tests":"idk"
    };
    res.send(message,links);
});

app.use("users",function(req,res){
    const message = "This is lab2!";
    const links = {
        "All users":" users",
        "Get": "need id"
    };
    res.send(message,links);
});

app.use("All users",function(req,res){
    const users = user_functions.getAll();
    if(users!== false){ 
       let listOfUsers = "";
       for(let user of users){
          listOfUsers += ` {${user.id}} - ${user.login} \r\n`;
       }
       res.send(listOfUsers);
       return;
    }
    res.send("Error with file");
});

app.use("Get",function(req,res){
    const fields = {
        "id":"Enter user id"
    };
    const nextState = "show user";
    
    const form = new InputForm(nextState,fields);
    let message = "Select user";
    if (req.data!==null){
        if(req.data.error !== null){
           message = req.data.error + "\r\n\r\n" + message;
        } 
    }
    res.send(message,form);
});

app.use("show user",function(req,res){
    const userId = parseInt(req.data.id);
    const userAtId = user_functions.getById(userId);
    if(!userAtId){
        let data = {
                error:"ERROR"
        };
        res.redirect("Get",data);
        return;
    }
    res.send(userAtId);
});


app.use("tests",function(req,res){
    const message = "This is lab2!";
    const links = {
        "all_test":" tests",
        "insert_test":" create and insert",
    };
    res.send(message,links);
});


app.use("all_test",function(req,res){

    const tests = test_functions.getAll();
    if(tests===false || tests===null || tests.length === 0){
        res.send("Error empty file or file didn't exist");
        return;
    }
    else{
        let listOfTests = "";
        for(let temp of tests){
            if(temp!==null){
                listOfTests += ` {${temp.id}} - ${temp.name} \r\n`;
            }
        }
        let link = {
            "get_test":"select one of all tests"
        }; 
        res.send(listOfTests,link);   
    }
}); 



app.use("insert_test",function(req,res){
        
    const fields = {

        "name": "Enter name",
        "questions": "Enter questions",
        "author": "Enter author",
        "picUrl": "Enter url",
        "subject": "Enter subject",
        "rating": "Enter rating",
        "numOfPeopleWhoPassed": "Enter number"
    
    };
    
    const nextState = "insertIntoTests";

    const form = new InputForm(nextState,fields);
    
    let message = "insert test"; 

    if(req.data!==null && typeof req.data.error !== "undefined"){
        message = req.data.error + "\r\n\r\n" + message;
     }

    res.send(message,form);
      
});

function validate(name,questions,author,picUrl,subject,rating,numOfPeopleWhoPassed){
    if(name.length < 30 && name.length >0 
        && questions.length >0 && picUrl.length > 0 
        && author.length < 30 && author.length >0
        && subject.length < 30 && subject.length >0  
        &&  parseFloat(rating)>0 && parseFloat(rating)<10
        && parseInt(numOfPeopleWhoPassed)>=0){
            return true;
        }
        return false;
};

app.use("insertIntoTests",function(req,res){
    const name = req.data.name;
    const questions = req.data.questions;
    const picUrl = req.data.picUrl;
    const author = req.data.author;
    const subject = req.data.subject;
    const rating = req.data.rating;
    const numOfPeopleWhoPassed = req.data.numOfPeopleWhoPassed;
    if(!validate(name,questions,picUrl,author,subject,rating,numOfPeopleWhoPassed)){
        
        let data = {
            error:"ERROR"
        };
        res.redirect("insert_test",data);
        return;
    }
    const test = new test_functions(name,questions,picUrl,author,subject,parseFloat(rating),parseInt(numOfPeopleWhoPassed));
    test_functions.insert(test);
    res.send(test);
});

/*
app.use("delete_test",function(req,res){
    const fields = {
        "id":"Enter user id"
    };
    const nextState = "delete user";
    const form = new InputForm(nextState,fields);
    res.send("Delete user",form);
});

app.use("delete test",function(req,res){
    const userId = parseInt(req.data.id);
    let isTrue = test_functions.delete(userId);
    
    if(!isTrue){
        res.redirect("delete_test");
        return;
    } 
    res.send("Deleted");
});

*/
app.use("get_test",function(req,res){
    
    const fields = {
        "id":"Enter test id"
    };
    const nextState = "get test";
    const form = new InputForm(nextState,fields);
    let message = "get test";
    if (req.data!==null){
        if(req.data.error !== undefined){
           message = req.data.error + "\r\n\r\n" + message;
        } 
        if(req.data.info !== undefined){
           message = req.data.info + "\r\n\r\n" + message;
        } 
    }
    res.send(message,form);
});

app.use("get test",function(req,res){
    const testId = parseInt(req.data.id);
    let temp = test_functions.getById(testId);
    
    if(temp === false){
        let data = {
            error:"ERROR"
        };
        res.redirect("get_test",data);
        return;
    } 
    const fields = {
        "update_test": {
            description: "edit current test",
            data: {
                testId: testId,
            }
        },
        "delete_test": {
            description: "delete choosen test",
            data: {
                testId: testId.toString(),
            }
        }
    };
    res.send(` {${temp.id}} - ${temp.name} \r\n`,fields);
});

app.use("delete_test",function(req,res){
    const testId = parseInt(req.data.testId);

    let isTrue = test_functions.delete(testId);
    let data;
    if(!isTrue){
        data = {
            error:"ERROR"
        };
    }else{
        data = {
            info:"Deleted"
        };
    }
    res.redirect("get_test",data);
});


app.use("update_test",function(req,res){
    const testId = parseInt(req.data.testId);
    const userAtId = test_functions.getById(testId);    
    const fields = {
        "id":{
            description:"Enter test Id",
            auto: testId.toString()
        },
        "name":{
            description:"Enter name",
            default: userAtId.name
        }, 
        "questions":{
            description:"Enter questions",
            default: userAtId.questions
        }, 
        "author":{
            description:"Enter author",
            default: userAtId.author
        }, 
        "picUrl":{
            description: "Enter url",
            default: userAtId.picUrl
        },
        "subject":{
            description: "Enter subject",
            default: userAtId.subject
        },
        "rating":{
            description: "Enter rating",
            default: userAtId.rating.toString()
        },
        "numOfPeopleWhoPassed":{
            description: "Enter number",
            default: userAtId.numOfpeopleWhoPassed.toString()
        }                  
    };

    /*const UpdateIntoTests = {
        description: "delete choosen test",
        data: {
            testId: testId,
        }
    };*/
    const nextState = "UpdateIntoTests"; 

    const form = new InputForm(nextState,fields);
    
    let message = "if you don't want to change press Enter";
    
    if(req.data!==null && req.data.error!== undefined){
        message = req.data.error + " \r\n\r\n"+ message; 
    }
    res.send(message,form);
  
});


app.use("UpdateIntoTests",function(req,res){
const name = req.data.name;
const questions = req.data.questions;
const picUrl = req.data.picUrl;
const author = req.data.author;
const subject = req.data.subject;
const rating = req.data.rating;
const numOfPeopleWhoPassed = req.data.numOfPeopleWhoPassed;
const data = {
    "testId": parseInt(req.data.id),
    "error":"one of parameters is wrong"
};
if(!validate(name,questions,picUrl,author,subject,rating,numOfPeopleWhoPassed)){
    res.redirect("update_test",data);
    return;
}

let test = new test_functions(name,questions,picUrl,author,subject,parseFloat(rating),parseInt(numOfPeopleWhoPassed));

test.id = parseInt(req.data.id);

let x = test_functions.update(test); 

if(x){
    res.send(test);
}else{
    res.send("Error");
}

});


/*
// const x = new user_functions.User(1,"Hiza",2,"dd","Dd",false);
    //let y = new x(1,"Hiza",2,"dd","Dd",false);
    

app.use("/",function(req,res){
    const message = "This is lab2!";
    res.send(message);
});

*/
const PORT = 3030;
app.listen(PORT);
browser.open(PORT);





